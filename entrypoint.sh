#!/bin/ash

[ -f /etc/wireguard/wg0.conf ] || touch /etc/wireguard/wg0.conf
/sbin/inotifyd /app/wg-restart /etc/wireguard/wg0.conf:w &
wg-quick up wg0

p=""
p=$p"--bind-address=${BIND_ADDRESS}"
[[ "${LOGIN_PAGE}" == 0 ]] && p=$p' --disable-login=true'
echo "Parameters: ${p}"
/app/wg-ui ${p}
